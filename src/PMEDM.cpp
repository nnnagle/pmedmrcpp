#include "PMEDM.h"
/*
//' Run the PMEDM solver
//' 
//' This function tests the PMEDM solver and returns a list with the result
//' 
//' @param X microdata matrix
//' @param Y summary table vector (probably normalized normalized by N to create fractions)
//' @param V variance-covariance matrix (probably normalized by n/N^2)
//' @param q prior design weights (probably normalized by N)
//' @param opt options list
//' @param lambda starting values (default=0)
//' @return List object, containing:
//' \item{f}{function value}
//' \item{grad}{gradient}
//' \item{p}{probability weights}
//' \item{pred}{prediction vector}
*/
// [[Rcpp::export]]
SEXP solve_(SEXP X_, SEXP Y_, SEXP V_, SEXP q_, SEXP opt_, SEXP lambda_){
  MSpMat X(Rcpp::as<MSpMat>(X_));
  MapVecd Y(Rcpp::as<MapVecd>(Y_));
  MSpMat V(Rcpp::as<MSpMat>(V_));
  MapVecd q(Rcpp::as<MapVecd>(q_));
  MapVecd lambda(Rcpp::as<MapVecd>(lambda_));
  PMEDM opt(X, Y, V, q);
  Rcpp::List opt_params(opt_);
  opt.init_state.update(lambda, opt);
  opt.precon_type = Rcpp::as<int>(opt_params["precon_type"]);
  opt.tr_iter = Rcpp::as<int>(opt_params["tr_iter"]);
  opt.tr_rad = Rcpp::as<double>(opt_params["tr_rad"]);
  opt.precon_reset = Rcpp::as<int>(opt_params["precon_reset"]);
  opt.tr_tol = Rcpp::as<double>(opt_params["tr_tol"]);
  opt.max_iter = Rcpp::as<double>(opt_params["max_iter"]);
  state soln = opt.trust_region(opt.init_state);
  return List::create(Named("f") = wrap(soln.f),
                      Named("grad") = wrap(soln.grad),
                      Named("p") = wrap(soln.p),
                      Named("pred") = wrap(soln.Xp),
                      Named("lambda") = wrap(soln.lambda)
                      );
}


VectorXd PMEDM::hessian_y(const VectorXd& y, const state& st){
  VectorXd Hy = X.adjoint()*(st.Dp*(X*y));
  Hy += V*y;
  Hy -= st.Xp * st.Xp.dot(y);
  return(Hy);
}


void PMEDM::set_preconditioner(const int& p_type, const state& st_){
  if(p_type==0){
    Rcpp::Rcout << "Using Default (Identity) Preconditioner" << std::endl;
    delete Pptr;
    Pptr = new preconditioner;
  } else if(p_type==1){
    Rcpp::Rcout << "Using Sparse Hessian Preconditioner" << std::endl;
    delete Pptr;
    Pptr = new preconditioner_sparse_hessian(st_, *this);
  }
}


state PMEDM::trust_region(state& st_){
  state st_cur = st_;
  state st_try = st_;
  VectorXd g_cur = st_cur.grad;
  VectorXd g_try = g_cur;
  VectorXd x_cur = st_cur.lambda;
  VectorXd x_try = x_cur;
  double ared, pred, r, phi_cur, phi_try;
  phi_cur = 0;
  Rcpp::Rcout << "Starting Trust Region Algorithm" << std::endl;
  Rcpp::Rcout << std::setw(5) << "iter" << std::setw(13) << "fun" 
    << std::setw(13) << "||grad||"
    << std::setw(13) << "||try step||"
    << std::setw(13) << "act red"
    << std::setw(13) << "pred red"
    << std::setw(11) << "radius"
    << std::setw(11) << "inner iter"
    << std::endl;

  int iter;
  for (int i=0; i<max_iter; i++) { //start main loop
    if(precon_ctr >= precon_reset){
      precon_ctr = 0;
      double old_size = st_cur.grad.dot(Pptr->mult_y(st_cur.grad));
      set_preconditioner(precon_type, st_cur);
      double new_size = st_cur.grad.dot(Pptr->mult_y(st_cur.grad));
      tr_rad *= (new_size / old_size);
      }
    double g_nrm = st_cur.grad.squaredNorm();
    // Test for convergence
    if(sqrt(g_nrm) < tr_tol){
      lambda_ = x_cur;
      st_cur.update(lambda_, *this);
      Rcpp::Rcout << std::setw(5) << i << " " << std::setw(12) << st_cur.f
        << " " << std::setw(12) << st_cur.grad.norm()
        << " " << std::setw(12) << " - "
        << " " << std::setw(12) << " - "
        << " " << std::setw(12) << " - "
        << " " << std::setw(10) << " - "
        << " " << std::setw(11) << " - "
        << std::endl;
      Rcpp::Rcout << "Solver ended with convergence" << std::endl;
      return(st_cur);
    }
    
    solve_tr(g_try, phi_try, st_cur, iter); // solve trust region subproblem
    x_try = x_cur.array() + g_try.array();
    st_try.update(x_try, *this);
    ared = st_cur.f - st_try.f;
    pred = phi_cur - phi_try;
    r = ared/pred;
    Rcpp::Rcout << std::setw(5) << i << " " << std::setw(12) << st_cur.f
      << " " << std::setw(12) << st_cur.grad.norm()
      << " " << std::setw(12) << sqrt(g_try.dot(Pptr->mult_y(g_try)))
      << " " << std::setw(12) << ared
      << " " << std::setw(12) << pred
      << " " << std::setw(10) << tr_rad
      << " " << std::setw(11) << iter
      << std::endl;
    if (r > 0 & ared > 0) {
      x_cur = x_try;
      st_cur = st_try;
    }
    if (r>.75 & ared > 0) { tr_rad *= 2. ;}
    if (r<.25 | pred < 0 | std::isnan(st_try.f)) { tr_rad *= .25;}  
    precon_ctr += 1;
  }
  Rcpp::Rcout << std::setw(5) << max_iter << " " << std::setw(12) << st_cur.f
    << " " << std::setw(12) << st_cur.grad.norm()
    << " " << std::setw(12) << " - "
    << " " << std::setw(12) << " - "
    << " " << std::setw(12) << " - "
    << " " << std::setw(10) << " - "
    << " " << std::setw(11) << " - "
    << std::endl;
  Rcpp::Rcout << "Solver ended after maxumum number of iterations" << std::endl;
  return(st_cur);
};

double PMEDM::find_tau(VectorXd& z, VectorXd& d){
//  double d2 = d.squaredNorm();
  VectorXd Pd = Pptr->mult_y(d);
  double d2 = d.dot(Pd);
//  double z2 = z.squaredNorm();
  double z2 = z.dot(Pptr->mult_y(z));
//  double zd = d.dot(z);
  double zd = z.dot(Pd);
  
  double root = zd*zd - d2*(z2 - (tr_rad*tr_rad));
  double tau = (sqrt(root) - zd) / d2;
  return(tau);
};

void PMEDM::solve_tr(VectorXd& s, double& phi, const state& st, int& iter){
  VectorXd gi = st.grad;
  s = gi;
  s.setZero();
  VectorXd vi = Pptr->solve(gi);
  VectorXd pi = -vi;
  double g_nrm;
  g_nrm = st.grad.squaredNorm();
  
  VectorXd s_try = s;
  VectorXd Hp = s;
  double pHp, alpha, beta, gv_old, gv_new, pi_nrm, tau;
  gv_old = gi.dot(vi);
  
  for(iter=0; iter<tr_iter; iter++){
    
    //Check for illegal value and exit    
    if (!std::isfinite(pi.norm())) { 
      phi = s.dot(st.grad) + .5 * s.dot( hessian_y(s, st) );
      s = s.array() + pi.array();
      return;
    }
    //Check for convergence and exit
    if(sqrt(vi.squaredNorm() / g_nrm) < tr_tol){
//    if(sqrt(gi.squaredNorm()/g_nrm) < std::min(.1,sqrt(sqrt(g_nrm)))){
      phi = s.dot(st.grad) + .5 * s.dot( hessian_y(s, st) );
      return;
    }
    Hp = hessian_y(pi, st);
    pHp = pi.dot(Hp);
    //Check for negative curvature and follow to trust region boundary
    if(pHp < 0) {
      Rcpp::Rcout << "Negative curvature found in trust region.  Is this correct?" << std::endl;
      tau = find_tau(s, pi);
      s = s.array() + pi.array()*tau;
      phi = s.dot(st.grad) + .5 * s.dot( hessian_y(s, st) );
      return;
    }
    alpha = gi.dot(vi) / pHp;
    s_try = s.array() + pi.array()*alpha;
    
    // IF Step Exits the Trust Region, stop
    if(s_try.dot(Pptr->mult_y(s_try)) > tr_rad){
      VectorXd pa = pi.array()*alpha;
      tau = find_tau(s, pa);
      s = s.array() + pi.array()*(tau*alpha);
      phi = s.dot(st.grad) + .5 * s.dot( hessian_y(s, st) );
      return;
    }
    s = s_try;
    gi = gi.array() + Hp.array()*alpha;
    vi = Pptr->solve(gi);
    gv_new = gi.dot(vi);
    beta = gv_new/gv_old;
    gv_old = gv_new;
    pi = -vi.array() + pi.array()*beta;
  }
  Rcpp::Rcout << "Exited at end." << std::endl;
  s = s.array() + pi.array();
  phi = s.dot(st.grad) + .5 * s.dot( hessian_y(s, st) );
  return;
  
};

// Helper functions
SpMat spDiag(const VectorXd& X) {
  SpMat D;
  D.resize( X.size(), X.size() );
  D.reserve(X.size());
  for(int i=0; i<X.size(); i++) {D.insert(i,i) = X.coeffRef(i); }
  return(D);
};


//Usual Constructors
  
PMEDM::PMEDM(const MSpMat& X_, const MapVecd& Y_, const MSpMat& V_, const MapVecd& q_) : 
    X(X_), Y(Y_), V(V_), q(q_), 
    tr_tol(pow(std::numeric_limits<double>::epsilon(), .5)),
    tr_iter(1000), tr_rad(1), precon_type(1), precon_ctr(0), precon_reset(20), max_iter(200) {
      lambda_ = Y;
      lambda_.setZero();
      init_state.update(lambda_, *this);
      //init_state = new_state(lambda_);
//      P = set_preconditioner(precon_type, init_state);
      Pptr = new preconditioner;
      set_preconditioner(precon_type, init_state);
//      Pptr = new preconditioner_sparse_hessian(init_state, *this);
//      Pptr = &P;
      //preconditioner* P0 = new preconditioner;
      //P0 = set_preconditioner(precon_type, init_state);
      Rcpp::Rcout << "Initializing PMEDM solver" << std::endl;
};

state PMEDM::new_state(const VectorXd lambda){
  VectorXd qXl = (-X*lambda).array().exp() * q.array();
  const VectorXd p = qXl / qXl.sum();
  const VectorXd Xp = X.transpose()*p;
  double lVl = lambda.dot(V*lambda);
  double fun = Y.dot(lambda) + log(qXl.sum()) + 0.5*lambda.dot(V*lambda);
  const VectorXd grad = Y + V*lambda - Xp;
  return(state(lambda, p, Xp, fun, grad));
}


