#include "PMEDM.h"
//
//  File:             preconditioner.cpp
//  Description:      Part of PMEDMrcpp package:  Implements the preconditioner
//  Created:          1 April, 2013
//  Author:           Nicholas Nagle
//  email:            <nnagle@utk.edu>
//
// 
//  Current preconditioners are
//    0 Default (identity) preconditioner (implemented as the base class)
//    1 Exact Sparse Hessian preconditioner (preconcitioner_sparse_hessian::)
//
//  The preconditioner is set within PMEDM::set_preconditioner()
//
//  The preconditioner class has three necessary functions: 
//    - P( ... ) :              a constructor
//    - mult_y(VectorXd& y) :   multiply by vectory y
//    - solve(VectorXd& y) :    solve for Px=b
//

// preconditioner_sparse_hessian::
// Uses the exact Hessian X'pX + V - X'pp'X
// Constructs and factorized the sparse X'pX + V, but implements the rank one downdate -X'pp'X
// via the Sherman Woodbury formula
preconditioner_sparse_hessian::preconditioner_sparse_hessian(const state& st, const PMEDM& PM):
  p(st.p), Xp(st.Xp), Dp(st.Dp), XPX(PM.X.adjoint()* Dp * PM.X + PM.V), chol(XPX) {
    //Rcpp::Rcout << "sum " << XPX.sum() << std::endl;
    //SpMat T =  (Dp.cwiseSqrt()*X);
    //SpChol chol(XPX);
    chol.analyzePattern(XPX);
    chol.factorize(XPX);
    if(chol.info() != Eigen::Success){
      Rcpp::Rcout << "Cholesky Factorization failed" << std::endl;
    }
  }

VectorXd preconditioner_sparse_hessian::mult_y(VectorXd& y_){
//  Rcpp::Rcout << "Inside preconditioner_sparse_hessian::mult_y " << std::endl;
  VectorXd Hy =  XPX*y_;
           Hy -= Xp * Xp.dot(y_);
  return(Hy);
}

VectorXd preconditioner_sparse_hessian::solve(VectorXd& y_){
  //Solve (A+uv')\y = A\y - (1+v'A\u)^{-1} (A\u v' A\y)
  // A:= XPX;    u:= Xp    v:= -Xp
  //chol.analyzePattern(XPX);
  //chol.factorize(XPX);
  VectorXd Ainv_y = chol.solve(y_);
  VectorXd Ainv_u = -chol.solve(Xp);
//  Rcpp::Rcout << "Inside sparse_hessian preconditioner" << std::endl;
//  Rcpp::Rcout << "sum A\(-Xp) " << Ainv_u.sum() << std::endl;
  double factor = 1/(1+Xp.dot(Ainv_u));
  VectorXd Hinv_y = Ainv_y;
  Hinv_y -=  Ainv_u * ((Xp.dot(Ainv_y) * factor));
  return(Hinv_y);
}

// preconditioner::
// Implements mult_y(y) and solve(y) merely by returning the address of y.
VectorXd preconditioner::mult_y(VectorXd& y_) {
  return(y_);
}

VectorXd preconditioner::solve(VectorXd& y_){
  return(y_);
}
