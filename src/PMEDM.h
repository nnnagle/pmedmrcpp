#ifndef _PMEDMrcpp_PMEDM_H
#define _PMEDMrcpp_PMEDM_H

#include <RcppEigen.h>
#include <iostream>
#include <iomanip>
#include <string>

using namespace Rcpp;
using namespace RcppEigen;

using Eigen::MatrixXd;
using Eigen::Map;
using Eigen::MatrixXd;                  // variable size matrix, double precision
using Eigen::VectorXd;                  // variable size vector, double precision
using Eigen::VectorXi;
using Eigen::SparseMatrix;

typedef Eigen::Map<VectorXd>             MapVecd;
typedef Eigen::Map<MatrixXd>             MapMatd;
typedef Eigen::MappedSparseMatrix<double> MSpMat;
typedef Eigen::SparseMatrix<double>        SpMat;
typedef Eigen::SimplicialLDLT<SpMat> SpChol;

class state;
class PMEDM;

class state {
  public:
    VectorXd lambda;
    VectorXd p;
    VectorXd Xp;
    double f;
    VectorXd grad;
    SpMat Dp;
    
  public:
    state();
    state(const VectorXd&, const VectorXd&, const VectorXd&, const double&, const VectorXd&);
    state(const VectorXd&, const PMEDM&);
    void update(const VectorXd, const PMEDM&);
    Rcpp::List print();
    
  private:
};

class preconditioner{
  public:
    virtual VectorXd mult_y(VectorXd&);
    virtual VectorXd  solve(VectorXd&);
    virtual ~preconditioner(){};
};

class preconditioner_sparse_hessian : public preconditioner {
  public:
    const VectorXd p;
    const VectorXd Xp;
    const SpMat Dp;
    SpMat XPX;
    SpChol chol;
  public:
    preconditioner_sparse_hessian(const state&, const PMEDM&);
    virtual VectorXd mult_y(VectorXd&);
    virtual VectorXd solve(VectorXd&);
};


class PMEDM {
public:
  PMEDM(const MSpMat&, const MapVecd&, const MSpMat&, const MapVecd&); //Usual Constructor
  state trust_region(state&);
  void set_preconditioner(const int&, const state&);
  const     MSpMat X;   // Sparse X (model) matrix
  const    MapVecd Y;   // Benchmark Y data
  const     MSpMat V;   // Sparse Benchmark Variance matrix   
  const    MapVecd q;   // Design weights q
  state init_state;



public: //Set to private: when you release
  VectorXd lambda_;     //Current parameter values
  double tr_tol;
  int tr_iter, precon_type, precon_ctr, precon_reset, max_iter;
  preconditioner P;
  preconditioner* Pptr;
  double tr_rad;
      
private:
  state new_state(const VectorXd);
  VectorXd hessian_y(const VectorXd& y, const state&);
  void solve_tr(VectorXd&, double&, const state&, int&);
  double find_tau(VectorXd&, VectorXd&);
  
};



SpMat spDiag(const VectorXd&);
RcppExport SEXP PMEDM__new(SEXP);

#endif