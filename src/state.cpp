#include "PMEDM.h"
//
//  File:             state.cpp
//  Description:      Part of PMEDMrcpp package:  Implements the state class
//  Created:          1 April, 2013
//  Author:           Nicholas Nagle
//  email:            <nnagle@utk.edu>
//
//  state:: is a class defining a state of the PMEDM.
//  It contains a vector lambda, as well as function and gradient values
//      and a few other things.
//


Rcpp::List state::print(){
  return List::create(Named("f") = wrap(f),
                      Named("grad") = wrap(grad),
                      Named("p") = wrap(p),
                      Named("pred") = wrap(Xp)
                      );
}


state::state(){
};


state::state(const VectorXd& lambda_, const PMEDM& PM_):
  lambda(lambda_) {
    VectorXd qXl = (-PM_.X*lambda).array().exp() * PM_.q.array();
    p = qXl / qXl.sum();
    Xp = PM_.X.adjoint() * p;
    f = PM_.Y.dot(lambda) + log(qXl.sum()) + 0.5*lambda.dot(PM_.V*lambda);
    grad = PM_.Y + PM_.V*lambda - Xp;
    Dp = spDiag(p);
}

void state::update(const VectorXd lambda_, const PMEDM& PM_){
    lambda = lambda_;
    VectorXd qXl = (-PM_.X*lambda).array().exp() * PM_.q.array();
    p = qXl / qXl.sum();
    Xp = PM_.X.adjoint() * p;
    f = PM_.Y.dot(lambda_) + log(qXl.sum()) + 0.5*lambda_.dot(PM_.V*lambda_);
    grad = PM_.Y + PM_.V*lambda - Xp;
    Dp = spDiag(p);
}

state::state(const VectorXd& lambda_, const VectorXd& p_, const VectorXd& Xp_, const double& fun_, const VectorXd& grad_): 
  lambda(lambda_), p(p_), Xp(Xp_), f(fun_), grad(grad_), Dp(spDiag(p_)){};
