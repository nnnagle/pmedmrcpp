PMEDMrcpp is a tool for solving the Penalized Maximum Entropy problem for Dasymetric Modeling (PMEDM), as described in Nagle et al 2014.

In order to solve this problem, you will need to specify your constraints in linear form:

$Y = Xw + e$, where $w$ are the unknown survey weights, and the errors $e$ have variance-covariance matrix $V$.

